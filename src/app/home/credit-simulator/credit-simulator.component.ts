import { Component, OnInit } from '@angular/core';
import { CreditSimulatorService } from 'src/app/service/credit-simulator.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormValidationService } from 'src/app/service/form-validation.service';
import { SweetAlertService } from 'src/app/service/sweet-alert.service';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-credit-simulator',
  templateUrl: './credit-simulator.component.html',
  styleUrls: ['./credit-simulator.component.scss']
})
export class CreditSimulatorComponent {

  constructor(
    private creditSimulatorService: CreditSimulatorService,
    private formBuilder: FormBuilder,
    private alert: SweetAlertService,
    private currencyPipe: CurrencyPipe,
  ) { }

  simulationResult: any = {};
  displayMessage = '';
  simulationForm: FormGroup = this.formBuilder.group({});

  ngOnInit(): void {
    this.simulationForm = this.formBuilder.group({
      RutCliente: ['', [Validators.required, FormValidationService.validateRut()]],
      MontoDelCredito: ['', [Validators.required, Validators.pattern(/^\d+$/),Validators.min(100000), Validators.max(999999999)]],
      NumeroCuotas: ['', [Validators.required, Validators.min(3), Validators.max(60)]]
    });
  }

  validField(field: string) {
    return this.simulationForm.controls[field].errors
      && this.simulationForm.controls[field].touched;
  }


  async simulateCredit() {
    try {
      if (this.simulationForm.invalid) {
        this.simulationForm.markAllAsTouched();
        return;
      }
      this.alert.loading('Procesando...');
      this.simulationResult = await this.creditSimulatorService.simulateCredit(this.simulationForm.value);
      setTimeout(() => {
        this.alert.close();
        this.alert.success('¡Crédito simulado exitósamente!');
        const { MontoDelCredito, NumeroCuotas } = this.simulationForm.value;
        const { ValorCuota } = this.simulationResult.response;

        let formattedValorCuota = this.currencyPipe.transform(ValorCuota, 'CLP', 'symbol', '1.0-0', 'es-CL');
        let formattedMontoDelCredito = this.currencyPipe.transform(MontoDelCredito, 'CLP', 'symbol', '1.0-0', 'es-CL');
        this.displayMessage = `Estimado cliente, el valor cuota según la simulación es ${formattedValorCuota }, al solicitar ${formattedMontoDelCredito} en ${NumeroCuotas} cuotas.`;
      }, 500);

    } catch (error) {
      this.alert.simpleError('Su solicitud no pudo ser procesada, por favor intente más tarde');
      console.log(error);
    }

  }

  reset() {
    this.simulationForm.reset();
    this.displayMessage = '';
  }
}
