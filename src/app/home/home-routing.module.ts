import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Importa tus componentes aquí
import { CreditSimulatorComponent } from './credit-simulator/credit-simulator.component';

const routes: Routes = [
  {
    path: '',
    component: CreditSimulatorComponent,
    children: [
      {
        path: 'credit-simulation',
        component: CreditSimulatorComponent,
      },
      // Más rutas de componentes hijo aquí si las necesitas
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
