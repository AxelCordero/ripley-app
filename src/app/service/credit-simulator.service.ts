import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CreditSimulatorService {

  private apiUrl = 'http://localhost:3010';

  constructor(private http: HttpClient) { }

  simulateCredit(simulationData: any): Promise<any> {
    const response = this.http.post(`${this.apiUrl}/credit/simulate`, simulationData);
    return firstValueFrom(response);
  }
}
