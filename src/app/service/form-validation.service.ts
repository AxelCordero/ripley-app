
import { AbstractControl, ValidatorFn } from '@angular/forms';

export class FormValidationService {

  constructor() { }

  static validateRut(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const value = (control.value || '').replace(/[.\-]/g, '');
      const body = value.slice(0, -1);
      const checkDigit = value.slice(-1).toUpperCase();

      if (body.length < 1 || body.length > 8 || Number.isNaN(Number(body)) || ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'K'].indexOf(checkDigit) < 0) {
        return { 'invalidRut': { value: control.value } };
      }

      let total = 0;
      let factor = 2;

      for (let i = body.length - 1; i >= 0; i--) {
        total += Number(body[i]) * factor;
        factor = factor === 7 ? 2 : factor + 1;
      }

      let dvExpected = 11 - (total % 11);
      let dvExpectedString: string;

      if (dvExpected === 11) {
        dvExpectedString = '0';
      } else if (dvExpected === 10) {
        dvExpectedString = 'K';
      } else {
        dvExpectedString = dvExpected.toString();
      }

      return dvExpectedString === checkDigit ? null : { 'invalidRut': { value: control.value } };
    };
  }
}
