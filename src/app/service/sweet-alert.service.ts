import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  constructor(private router: Router) { }

  errorGeneralReload(text = '') {
    Swal.fire({
      icon: 'error',
      title: (text === '') ? 'Ocurrio un problema!' : text,
      text: '',
      confirmButtonColor: '#510289',
      confirmButtonText: 'Recargar',
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.reload();
      }
    });
  }

  simpleError(text = '') {
    Swal.fire({
      icon: 'error',
      title: (text === '') ? 'Ocurrio un problema!' : text,
    });
  }

  loading(title = ''): Promise<any> {
    return Swal.fire({
      title: (title === '') ? 'Procesando...' : title,
      allowOutsideClick: false,
      allowEscapeKey: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }


  autocloseSuccess(title = '') {
    Swal.fire({
      icon: 'success',
      title: (title === '') ? 'Proceso realizado con éxito' : title,
      showConfirmButton: false,
      timer: 1500
    });
  }


  success(title = ''): Promise<any> {
    return Swal.fire({
      icon: 'success',
      title: (title === '') ? 'Proceso realizado con éxito' : title,
      confirmButtonColor: '#510289',
      confirmButtonText: 'OK',
      allowOutsideClick: false,
      allowEscapeKey: false,
    });
  }
  close() {
    Swal.close();
  }


}
